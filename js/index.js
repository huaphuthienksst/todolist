import { Item } from "../models/listToDo.js";
import { callApi } from "../utils/callApi.js";
const getEle = (id) => document.getElementById(id);
//let uncompletedList = new ListToDo();
//let completedList = new ListToDo();

const renderUncompletedList = (data) => {
  let contentHTML = "";
  for (let i in data) {
    contentHTML += `
        <li>
            <span>${data[i].content}</span>
            <div class="buttons">
            <button onclick="deleteUncompletedElement('${data[i].id}')" class="remove">
                <i class="far fa-trash-alt"></i>
            </button>
            <button onclick="moveToCompletedList('${data[i].id}')" class="complete">
                <i class="far fa-check-circle"></i>
            </button>
            </div>
        </li>
        `;
  }
  getEle("todo").innerHTML = contentHTML;
};
const renderCompletedList = (data) => {
  let contentHTML = "";
  for (let i in data) {
    contentHTML += `
        <li>
            <span>${data[i].content}</span>
            <div class="buttons">
            <button onclick="deleteCompletedElement('${data[i].id}')" class="remove">
                <i class="far fa-trash-alt"></i>
            </button>
            <button onclick="moveToUncompletedList('${data[i].id}')" class="complete">
                <i class="fas fa-check-circle"></i>
            </button>
            </div>
        </li>
        `;
  }
  getEle("completed").innerHTML = contentHTML;
};
const getListTask = () => {
  callApi("Completed", "GET", null)
    .then((res) => {
      console.log(res.data);
      renderCompletedList(res.data);
    })
    .catch((err) => {
      console.log(err);
    });
  callApi("Uncompleted", "GET", null)
    .then((res) => {
      console.log(res.data);
      renderUncompletedList(res.data);
    })
    .catch((err) => {
      console.log(err);
    });
};
getListTask();

getEle("addItem").addEventListener("click", () => {
  let newItemContent = getEle("newTask").value;
  getEle("newTask").value = "";
  //onsole.log(newItem);
  const newItem = new Item("", newItemContent);
  callApi("Uncompleted", "POST", newItem)
    .then((res) => {
      console.log(res);
      getListTask();
    })
    .catch((err) => {
      console.log(err);
    });
});

const deleteUncompletedElement = (id) => {
  callApi(`Uncompleted/${id}`, "DELETE", null)
    .then((res) => {
      console.log(res);
      getListTask();
    })
    .catch((err) => {
      console.log(err);
    });
};
window.deleteUncompletedElement = deleteUncompletedElement;

const deleteCompletedElement = (id) => {
  callApi(`Completed/${id}`, "DELETE", null)
    .then((res) => {
      console.log(res);
      getListTask();
    })
    .catch((err) => {
      console.log(err);
    });
};
window.deleteCompletedElement = deleteCompletedElement;

const moveToCompletedList = (id) => {
  callApi(`Uncompleted/${id}`, "GET", null)
    .then((res) => {
      //console.log(res.data);
      const item = new Item("", res.data.content);
      return callApi(`Completed`, "POST", item);
    })
    .then((res) => {
      console.log(res.data);
      return callApi(`Uncompleted/${id}`, "DELETE", null);
    })
    .then((res) => {
      console.log(res);
      getListTask();
    })
    .catch((err) => {
      console.log(err);
    });
};
window.moveToCompletedList = moveToCompletedList;

const moveToUncompletedList = (id) => {
  callApi(`Completed/${id}`, "GET", null)
    .then((res) => {
      //console.log(res.data);
      const item = new Item("", res.data.content);
      return callApi(`Uncompleted`, "POST", item);
    })
    .then((res) => {
      console.log(res.data);
      return callApi(`Completed/${id}`, "DELETE", null);
    })
    .then((res) => {
      console.log(res);
      getListTask();
    })
    .catch((err) => {
      console.log(err);
    });
};
window.moveToUncompletedList = moveToUncompletedList;
