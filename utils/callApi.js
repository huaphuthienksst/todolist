import { API_URL } from "../config/constant.js";

const callApi = (uri, method, data) => {
  return axios({
    url: `${API_URL}/${uri}`,
    method,
    data,
  });
};

export { callApi };
