class Item {
  constructor(id, content) {
    this.id = id;
    this.content = content;
  }
}
export { Item };
